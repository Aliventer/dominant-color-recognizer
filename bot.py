from cv2_dominant import get_dominant
import config

import asyncio
import discord
import aiohttp
import os
import time

try:
    # uvloop работает только на Линуксах
    import uvloop
except ImportError:
    pass
else:
    asyncio.set_event_loop_policy(uvloop.EventLoopPolicy())


client = discord.Client()

async def fetch(session: aiohttp.ClientSession, url: str, filename: str) -> None:
    """Сохраняет изображение в файл"""
    async with session.get(url) as response:
        with open(filename, 'wb') as f:
            while True:
                # Записывает файл "кусками" по 64*1024 (оптимизация для больших изображений)
                chunk = await response.content.read(64*1024)
                if not chunk:
                    break
                f.write(chunk)

@client.event
async def on_ready():
    print(f'Successfully logged in as {client.user}')

@client.event
async def on_message(message):
    if message.content.lower().startswith('cv.image'):
        url = message.content.replace('<', '').replace('>', '').split()
        if len(url) == 1:
            if message.attachments:
                url = message.attachments[0].url
            else:
                url = str(message.author.avatar_url)
        else:
            url = url[1]

        ext = url.split('.')[-1].split('?')[0]
        filename = 'images/' + 'temp.' + ext

        # https://www.owasp.org/index.php/Unrestricted_File_Upload
        if ext not in ['jpg', 'jpeg', 'png', 'webp']:
            return await message.channel.send('Unsupported format')

        async with aiohttp.ClientSession() as session:
            await fetch(session, url, filename)

        now = time.time()
        color = discord.Colour.from_rgb(*await get_dominant(filename, 8))
        em = discord.Embed(color=color)
        em.set_image(url=url)
        em.set_footer(text=f'Time elapsed: {round(time.time() - now, 2)} seconds')

        await message.channel.send(embed=em)
        os.remove(filename)

client.run(config.token)
