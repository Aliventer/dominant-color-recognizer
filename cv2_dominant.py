import numpy as np
import cv2
import colorsys


async def get_dominant(filename, K):
    img = cv2.imread(filename)
    img = cv2.resize(img, (64, 64), interpolation=cv2.INTER_AREA)
    img = img.reshape((-1, 3))
    img = np.float32(img)

    criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 10, 1.0)
    _, _, centers = cv2.kmeans(img, K, None, criteria, 10, cv2.KMEANS_PP_CENTERS)

    return await mx_color(centers)

async def compare(c1, c2):
    # c[0] - Hue, c[1] - Saturation, c[2] - Value
    # Если Saturation изображения равен нулю, то он не учитывается
    formula = lambda c: c[2] ** 2
    if c1[1] or c2[1]:
        formula = lambda c: c[1] * c[2] ** 2

    return c1 if formula(c1) > formula(c2) else c2

async def mx_color(centers):
    color = [-1, -1, -1]
    for center in centers:
        # center хранит цвет в BGR, так что для перехода в RGB нужно читать массив в обратном порядке
        center = colorsys.rgb_to_hsv(*map(int, center[::-1]))
        color = await compare(color, center)

    return map(int, colorsys.hsv_to_rgb(*color))
