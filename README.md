#### Dominant Color Recognizer
Бот для дискорда, определяющий самый контрастный доминантный цвет на изображении через алгоритм кластеризации "k-means clustering".
#### А как запустить?
Установи Python 3.6+ и выполняй эти команды по очереди (инструкция только для Линуксоидов):
- `git clone https://gitlab.com/Aliventer/dominant-color-recognizer.git && cd dominant-color-recognizer`
- `pip install virtualenv`
- `python3 -m venv venv`
- `source venv/bin/activate`
- `pip install -r requirements.txt`
- Переименуй `example_config.py` в `config.py` и укажи там токен своего бота
- `python3 bot.py`
#### А как использовать?
Добавь бота к себе на сервер и напиши команду `cv.image <url>` (можно без `<>`). Посмотри сырцы, чтобы понять логику использования лучше.
